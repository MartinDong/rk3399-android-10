#!/bin/bash

BASE_DIR=vendor/google/gapps

#----------------------------------------------------------

function generate_head()
{
	cat > $1 <<- EOF
	#
	# Copyright 2016 The Android Open Source Project
	#
	# Licensed under the Apache License, Version 2.0 (the "License");
	# you may not use this file except in compliance with the License.
	# You may obtain a copy of the License at
	#
	#      http://www.apache.org/licenses/LICENSE-2.0
	#
	# Unless required by applicable law or agreed to in writing, software
	# distributed under the License is distributed on an "AS IS" BASIS,
	# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	# See the License for the specific language governing permissions and
	# limitations under the License.
	#
	EOF
}

function generate_device_mk()
{
	[ -z $1 ] && return -1

	echo -n "Generating $1..."

	generate_head $1
	printf "\n# GApps dependencies\n" >> $1
	printf "PRODUCT_COPY_FILES +=" >> $1

	for sf in $(find system/ optional/ -type f ! -name "*.apk"); do
		echo " \\" >> $1
		printf "\t${BASE_DIR}/proprietary/$sf:$sf:google" >> $1
	done
	printf "\n\n" >> $1

	printf "PRODUCT_PACKAGES +=" >> $1
	for sf in $(find system/ optional/ -type f -name "*.apk"); do
		apk_name=$(basename $sf)
		module_name=${apk_name//\.apk/}

		echo " \\" >> $1
		printf "\t${module_name}" >> $1
	done
	printf "\n\n" >> $1

	echo "done."
}

function generate_android_mk()
{
	[ -z $1 ] && return -1

	echo -n "Generating $1..."

	generate_head $1
	printf "\nLOCAL_PATH := \$(call my-dir)\n\n" >> $1

	for sf in $(find system/ optional/ -type f -name "*.apk"); do
		apk_name=$(basename $sf)
		module_name=${apk_name//\.apk/}
		if [[ $sf == */priv-app/* ]]; then
			module_path='$(TARGET_OUT_APPS_PRIVILEGED)'
		else
			module_path='$(TARGET_OUT_APPS)'
		fi

		cat >> $1 <<- EOF
		include \$(CLEAR_VARS)
		LOCAL_MODULE := $module_name
		LOCAL_MODULE_PATH := $module_path
		LOCAL_MODULE_SUFFIX := \$(COMMON_ANDROID_PACKAGE_SUFFIX)
		LOCAL_MODULE_CLASS := APPS
		LOCAL_MODULE_OWNER := google
		LOCAL_MODULE_TAGS := optional
		LOCAL_BUILT_MODULE_STEM := package.apk
		LOCAL_SRC_FILES := $sf
		LOCAL_CERTIFICATE := PRESIGNED
		include \$(BUILD_PREBUILT)

		EOF
	done

	echo "done."
}

#----------------------------------------------------------

generate_head       ../BoardConfigPartial.mk
generate_device_mk  ../device-partial.mk
generate_android_mk  ./Android.mk

